<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::post('/', 'AuthController@login')->name('checkLogin');

Route::prefix('meja')->middleware('auth')->group(function () {
    Route::get('main', 'MejaController@index')->name('indexMeja');
    Route::post('store', 'MejaController@store')->name('storeMeja');
});

Route::prefix('menu')->middleware('auth')->group(function () {
    Route::get('main', 'MenuController@index')->name('indexMenu');
    Route::get('main/{id}', 'MenuController@show')->name('showMenu');
    Route::post('store', 'MenuController@store')->name('storeMenu');
    Route::post('add/{idPesanan}/{idMenu}', 'MenuController@add')->name('addMenuPesanan');
    Route::get('delete/{id}', 'MenuController@destroy')->name('deleteMenu');
    Route::get('edit/{id}', 'MenuController@edit')->name('editMenu');
    Route::put('update/{id}', 'MenuController@update')->name('updateMenu');
});

Route::prefix('pesanan')->middleware('auth')->group(function () {
    Route::get('main', 'PesananController@index')->name('indexPesanan');
    Route::post('store', 'PesananController@store')->name('storePesanan');
    Route::get('delete/{id}', 'PesananController@destroy')->name('deletePesanan');
    Route::get('edit/{id}', 'PesananController@edit')->name('editPesanan');
});

Route::prefix('transaksi')->middleware('auth')->group(function () {
    Route::get('main', 'TransaksiController@index')->name('indexTransaksi');
    Route::post('store/{id}', 'TransaksiController@store')->name('storeTransaksi');
    Route::get('delete/{id}', 'PesananController@destroy')->name('deletePesanan');
    Route::get('edit/{id}', 'PesananController@edit')->name('editPesanan');
});