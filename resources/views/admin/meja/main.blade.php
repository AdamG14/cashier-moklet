<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CashierMoklet</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Meja
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: -10px;">
          <a href="admin/project/list"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Kasir</a></li>
        <li class="active">Meja</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <div class="row"> 
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tableMeja">
                  <thead>
                  <style>th, td{text-align: center;}</style>
                  <tr>
                    <th>Nomor</th>
                    <th class="col-md-12">Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($meja as $m)
                  <tr>
                    <td>{{ $m->id }}</td>
                    @if($m->status == 0)
                      <td><span class="label label-success">Tersedia</span></td>
                    @endif
                    @if($m->status == 1)
                      <td><span class="label label-danger">Ditempati</span></td>
                    @endif
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
$(function () {
    $('#tableMeja').DataTable( {
      "columnDefs" : [
        { 'orderable': false, 'targets': [1] }
      ]
    });
  })

  $(document).ready(function(){
    $('.alert-success').fadeIn().delay(1000).fadeOut();
  });
</script>
</body>
</html>