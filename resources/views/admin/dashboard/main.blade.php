<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DPC | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard <small>{{ Carbon\Carbon::now()->format('d F Y')}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> DPC</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Company Value</h4>
              <h3>Rp. {{ number_format($companyValue, 2, ",", ".") }}</h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Company Employees</h4>
              <h3>{{ count($user) }}</h3>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{ count($project) }}</h3>

              <p>Total Project</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-gear"></i>
            </div>
            <a href="{{ route('listProject') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ count($po) }}</h3>

              <p>Total Payment</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-cart"></i>
            </div>
            <a href="{{ route('listPO') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ count($notes) }}</h3>

              <p>Total Notes</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-paper"></i>
            </div>
            <a href="{{ route('listNotes') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Current Project</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th class="col-md-6">Location</th>
              <th class="col-md-3">Start</th>
              <th class="col-md-3">Deadline</th>
            </tr>
            </thead>
            <tbody>
              @foreach($project as $key => $p)
                @if($key < 5)
                <tr>
                    <td>{{ $p->location }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->start_project)->format('d F Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->end_project)->format('d F Y') }}</td>
                </tr>
                @endif
              @endforeach
            </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Current Purchase</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="col-md-4">Number</th>
                <th class="col-md-4">Value</th>
                <th class="col-md-4">Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach($po as $key => $p)
                @if($key < 8)
                <tr>
                    <td>{{ $p->number }}</td>
                    <td>{{ $p->value }}</td>
                    <td>{{ \Carbon\Carbon::parse($p->date)->format('d F Y') }}</td>
                </tr>
                @endif
              @endforeach
            </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Current Notes</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th class="col-md-3">Location</th>
                    <th class="col-md-3">User</th>
                    <th class="col-md-3">Title</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($notes as $key => $n)
                    @if($key < 3)
                    <tr>
                        <td>{{ $n->project->location }}</td>
                        <td>{{ $n->user->name }}</td>
                        <td>{{ $n->title }}</td>
                    </tr>
                    @endif
                  @endforeach
                </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <section class="content-header" style="margin-top: -2%;">
      <h1>
        Project Status
      </h1>
    </section>
    <section class="content container-fluid" style="margin-bottom: -10%;">
      <div class="row">
        <div class="col-md-4">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Done</span>
              <span class="info-box-number">{{ count($done) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($done) }}%"></div>
              </div>
                  <span class="progress-description">
                    {{ substr(100 / count($project) * count($done), 0, 2) }}% of project are done
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-spinner"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">On Progress</span>
              <span class="info-box-number">{{ count($progress) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($progress) }}%"></div>
              </div>
                  <span class="progress-description">
                  {{ substr(100 / count($project) * count($progress), 0, 2) }}% of project are in progress
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending</span>
              <span class="info-box-number">{{ count($pending) }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{ 100 / count($project) * count($pending) }}%"></div>
              </div>
                  <span class="progress-description">
                  {{ substr(100 / count($project) * count($pending), 0, 2) }}% of project are pending
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
    </section>
    <section class="content-header">
      <h1>
        User Activity
      </h1>
    </section>
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-3">
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="ion ion-ios-eye"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Guest Visitor</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $guest }}</span>

              <span class="progress-description">
                    Visits Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Employee Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $employee }}</span>

              <span class="progress-description">
                    Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Admin Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $admin }}</span>

              <span class="progress-description">
                  Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-user-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Directur Activity</span>
              <span class="info-box-number" style="font-size: 30px;">{{ $director }}</span>

              <span class="progress-description">
                  Activity Today
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
    </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->

<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

</body>
</html>