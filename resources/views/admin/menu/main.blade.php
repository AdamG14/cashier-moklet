<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CashierMoklet</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(request()->is('menu/main/*') == true)
          Pilih Menu
        @else
          Daftar Menu
        @endif
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: -10px;">
          <a href="admin/project/list"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Kasir</a></li>
        <li class="active">Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    @if(request()->is('menu/main/*') == true)
    <div class="modal fade" id="addTransaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="margin-top: 10%;">
        <div class="modal-body">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Detail pembayaran</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table>
                <style>tr td{padding:10px; text-align: left;}</style>
                <tr>
                  <td>Id Pesanan</td>
                  <td>:</td>
                  <td>{{$pesanan->id}}</td>
                </tr>
                <tr>
                  <td>Nama Pelanggan</td>
                  <td>:</td>
                  <td>{{$pesanan->nama_pelanggan}}</td>
                </tr>
                <tr>
                  <td>Meja</td>
                  <td>:</td>
                  <td>{{$pesanan->meja_id}}</td>
                </tr>
                <tr>
                  <td>Daftar Pesanan</td>
                  <td>:</td>
                  <td>
                  @php $total = 0; @endphp
                  @foreach($detailPesanan as $dp)
                  @php $total += $dp->jumlah * $dp->menu->harga @endphp
                  - {{$dp->menu->nama}} | Qty : {{$dp->jumlah}} | Harga : Rp. {{number_format($dp->jumlah * $dp->menu->harga, 0, 0, '.')}} <br>
                  @endforeach
                  </td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td>:</td>
                  <td>Rp. {{number_format($total, 0, 0, '.')}}</td>
                </tr>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
        <form method="post" id="form-tambah-transaksi">
          {{ csrf_field() }}
          {{ method_field('POST') }}
          <div class="form-group">
            <label>Pembayaran</label>
            <input type="text" class="form-control" name="bayar" id="rupiah2">
            <input type="hidden" value="{{$total}}" name="total">
          </div>
          <button type="submit" class="btn btn-block btn-primary">Bayar</button>
        </form>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
      <div class="col-md-8">
            <div class="box">
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover" id="tableMenu">
                    <thead>
                    <style>th, td{text-align: center;}</style>
                    <tr>
                      <th>Nomor</th>
                      <th class="col-md-5">Nama</th>
                      <th class="col-md-5">Harga</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menu as $key => $m)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $m->nama }}</td>
                      <td>Rp. {{ number_format($m->harga, 0, '', '.') }}</td>
                      <td>
                        <a href="" data-toggle="modal" data-target="#addPesananMenu" onclick="add_pesanan_menu('{{$id}}', '{{$m->id}}')" class="btn btn-success"><i class="fa fa-plus"></i></a> 
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Detail pesanan</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table>
                <style>tr td{padding:10px; text-align: left;}</style>
                <tr>
                  <td>Id Pesanan</td>
                  <td>:</td>
                  <td>{{$pesanan->id}}</td>
                </tr>
                <tr>
                  <td>Nama Pelanggan</td>
                  <td>:</td>
                  <td>{{$pesanan->nama_pelanggan}}</td>
                </tr>
                <tr>
                  <td>Meja</td>
                  <td>:</td>
                  <td>{{$pesanan->meja_id}}</td>
                </tr>
                <tr>
                  <td>Daftar Pesanan</td>
                  <td>:</td>
                  <td>
                  @foreach($detailPesanan as $dp)
                  - {{$dp->menu->nama}} | Qty : {{$dp->jumlah}} <br>
                  @endforeach
                  </td>
                </tr>
                </table>
                <a href="" data-toggle="modal" data-target="#addTransaksi" onclick="tambah_transaksi('{{$pesanan->id}}')" style="margin-left: 90%;" class="btn btn-success"><i class="fa fa-arrow-right"></i></a>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
    </div>
    @else
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tableMenu">
                  <thead>
                  <style>th, td{text-align: center;}</style>
                  <tr>
                    <th>Nomor</th>
                    <th class="col-md-5">Nama</th>
                    <th class="col-md-5">Harga</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($menu as $key => $m)
                  <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $m->nama }}</td>
                    <td>Rp. {{ number_format($m->harga, 0, '', '.') }}</td>
                    <td>
                      <a href="{{ route('editMenu', $m->id) }}" class="btn btn-primary" style="margin: 3px;"><i class="fa fa-pencil"></i></a>
                      <a href="{{ route('deleteMenu', $m->id) }}" class="btn btn-danger"  style="margin: 3px;"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    @endif
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>

<!-- Modal -->
<div class="modal fade" id="addPesananMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 30%;">
      <div class="modal-body">
      <form method="post" id="form-tambah-pesanan-menu">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <h1 style="text-align: center;">Jumlah</h1>
          <input type="text" class="form-control" name="jumlah">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<!-- ./wrapper -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#tableMenu').DataTable( {
      "columnDefs" : [
        { 'orderable': false, 'targets': [1, 2, 3] }
      ]
    });
  })

  $(document).ready(function(){
    $('.alert-success').fadeIn().delay(1000).fadeOut();
  });

  function add_pesanan_menu(idPesanan, idMenu){
  $("#form-tambah-pesanan-menu").attr("action", "/menu/add/" + idPesanan + "/" + idMenu);
  }

  function tambah_transaksi(idPesanan){
  $("#form-tambah-transaksi").attr("action", "/transaksi/store/" + idPesanan);
  }

  var rupiah = document.getElementById('rupiah2');
rupiah.addEventListener('keyup', function(e){
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  rupiah.value = formatRupiah(this.value);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
</script>
</body>
</html>