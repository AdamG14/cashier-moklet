<aside class="main-sidebar">

    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><b>DASHBOARD</b></li>
        <!-- Meja -->
        <li class="{{ (request()->is('meja/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-table"></i> <span>Meja</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('meja/main')) ? 'active' : '' }}"><a href="{{ route('indexMeja') }}"><i class="fa fa-list"></i> <span>Daftar Meja</span></a></li>
            <li><a href="" data-toggle="modal" data-target="#addMeja" onclick="tambah_meja()"><i class="fa fa-plus"></i>Tambah Meja</a></li>
          </ul>
        </li>
        <!-- Menu -->
        <li class="{{ (request()->is('menu/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-cutlery"></i> <span>Menu</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('menu/main')) ? 'active' : '' }}"><a href="{{ route('indexMenu') }}"><i class="fa fa-list"></i> <span>Daftar Menu</span></a></li>
            <li><a href="" data-toggle="modal" data-target="#addMenu" onclick="tambah_menu()"><i class="fa fa-plus"></i>Tambah Menu</a></li>
          </ul>
        </li>
        <!-- Pesanan -->
        <li class="{{ (request()->is('pesanan/*')) ? 'treeview active' : 'treeview' }}">
          <a href="#"><i class="fa fa-book"></i> <span>Pesanan</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (request()->is('pesanan/main')) ? 'active' : '' }}"><a href="{{ route('indexPesanan') }}"><i class="fa fa-list"></i> <span>Daftar Pesanan</span></a></li>
            <li><a href="" data-toggle="modal" data-target="#addPesanan" onclick="tambah_pesanan()"><i class="fa fa-plus"></i>Tambah Pesanan</a></li>
          </ul>
        </li>
        <!-- Transaksi -->
          <li class="{{ (request()->is('transaksi/main')) ? 'active' : '' }}"><a href="{{ route('indexTransaksi') }}"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span></a></li>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

<!-- Modal -->
<div class="modal fade" id="addMeja" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 30%; margin-left: -58%; width: 300px;">
      <div class="modal-body">
      <form method="post" id="form-tambah-meja">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <label>Jumlah Meja</label>
          <input type="text" class="form-control" name="jumlah">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 10%; border-radius: 10px;">
      <div class="modal-header">
        <h1 class="modal-title" style="text-align: center;">Tambah Menu</h1>
      </div>
      <div class="modal-body">
      <form method="post" id="form-tambah-menu">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <label>Nama Menu</label>
          <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
          <label>Harga</label>
          <input type="text" class="form-control" name="harga" id="rupiah">
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addPesanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: 10%; border-radius: 10px;">
      <div class="modal-header">
        <h1 class="modal-title" style="text-align: center;">Buat Pesanan Baru</h1>
      </div>
      <div class="modal-body">
      <form method="post" id="form-tambah-pesanan">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <label>Nama Pelanggan</label>
          <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
          <label>Meja</label>
          <select name="meja" id="" class="form-control">
            <option value="">Pilih meja yang tersedia</option>
          @foreach(\App\Meja::where('status', '=', '0')->get() as $meja)
            <option value="{{$meja->id}}">{{$meja->id}}</option>
          @endforeach
          </select>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Submit</button>
      </form>
      </div>
    </div>
  </div>
</div>

<script>

function tambah_meja(){
  $("#form-tambah-meja").attr("action", "/meja/store/");
}

function tambah_menu(){
  $("#form-tambah-menu").attr("action", "/menu/store/");
} 

function tambah_pesanan(){
  $("#form-tambah-pesanan").attr("action", "/pesanan/store/");
}

var rupiah = document.getElementById('rupiah');
rupiah.addEventListener('keyup', function(e){
  // tambahkan 'Rp.' pada saat form di ketik
  // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
  rupiah.value = formatRupiah(this.value);
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
</script>