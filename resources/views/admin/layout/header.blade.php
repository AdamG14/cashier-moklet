  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{{ route('indexMeja') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>M</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Kasir</b> Moklet</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->