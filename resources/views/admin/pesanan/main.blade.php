<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CashierMoklet</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/bower_components/admin-lte/dist/css/skins/skin-black.min.css">
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-black sidebar-collapse">
<div class="wrapper">
@include('admin.layout.header')
@include('admin.layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin-top: 10px; margin-bottom: -10px;">
          <a href="admin/project/list"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Kasir</a></li>
        <li class="active">Pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tablePesanan">
                  <thead>
                  <style>th, td{text-align: center;}</style>
                  <tr>
                    <th>Nomor</th>
                    <th class="col-md-3">Nama Pelanggan</th>
                    <th class="col-md-3">Pesanan</th>
                    <th class="col-md-2">Jumlah</th>
                    <th class="col-md-1">Total</th>
                    <th class="col-md-1">Meja</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($pesanan as $key => $p)
                  <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $p->nama_pelanggan }}</td>
                    <td>
                    @foreach($detailPesanan as $dp)
                      @if($dp->pesanan_id == $p->id)
                        {{ $dp->menu['nama'] }}<br>
                      @endif
                    @endforeach
                    </td>
                    <td>
                    @foreach($detailPesanan as $dp)
                      @if($dp->pesanan_id == $p->id)
                        {{ $dp->jumlah }}<br>
                      @endif
                    @endforeach
                    </td>
                    <td>
                    @php
                      $jumlah = 0;
                      foreach($detailPesanan as $dp){
                        if($dp->pesanan_id == $p->id){
                          $jumlah = $jumlah + $dp->jumlah;
                        }
                      }
                    @endphp
                    {{ $jumlah }}
                    </td>
                    <td>{{ $p->meja_id }}</td>
                    @if($p->status == true)
                    <td><label class="label label-success">Paid</label></td>
                    @endif
                    @if($p->status == false)
                    <td><label class="label label-danger">Pending</label></td>
                    @endif
                    @if($p->status == false)
                    <td><a href="{{ route('showMenu', $p->id) }}" class="btn btn-primary" style="margin: 3px;"><i class="fa fa-pencil"></i></a></td>
                    @endif
                    <td>
                      
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin.layout.footer')
</div>
<!-- ./wrapper -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#tablePesanan').DataTable( {
      "columnDefs" : [
        { 'orderable': false, 'targets': [1, 2, 3, 4, 7 , 6] }
      ]
    });
  })

  $(document).ready(function(){
    $('.alert-success').fadeIn().delay(1000).fadeOut();
  });
</script>
</body>
</html>