<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Pesanan;
use App\Meja;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = Transaksi::all();

        return view('admin.transaksi.main', compact(['transaksi']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            "bayar" => "required",
        ]);

        $transaksi = new Transaksi;
        $transaksi->pesanan_id  = $id;
        $transaksi->total       = $request->total;
        
        $bayar       = $request->bayar;
        if(strpos($bayar, '.') == true) {
            $bayarReal = str_replace('.', '', $bayar);
            $transaksi->bayar = $bayarReal;
        } else {
            $transaksi->bayar = $bayarReal;
        }

        $pesanan = Pesanan::find($id);
        $pesanan->status = true;

        $meja   = Meja::find($pesanan->meja_id);
        $meja->status = 0;

        $meja->save();
        $pesanan->save();
        $transaksi->save();

        return redirect()->route('indexTransaksi')->with('alert', 'Transaksi berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
