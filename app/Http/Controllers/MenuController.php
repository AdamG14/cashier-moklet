<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\DetailPesanan;
use App\Pesanan;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::all();

        return view('admin.menu.main', compact(['menu']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "nama" => "required",
            "harga" => "required"
        ]);

        $menu = new Menu;
        $menu->nama = $request->nama;

        $harga = $request->harga;
        if(strpos($harga, '.') == true) {
            $hargaDb = str_replace('.', '', $harga);
            $menu->harga = $hargaDb;
        } else {
            $menu->harga = $request->harga;
        }

        $menu->save();

        return redirect()->route('indexMenu')->with('alert', 'Berhasil tambah menu');
    }

    public function add(Request $request, $idPesanan, $idMenu)
    {
        $this->validate($request, [
            "jumlah" => "required|min:1",
        ]);

        $detail = new DetailPesanan;
        $detail->menu_id    = $idMenu;
        $detail->jumlah     = $request->jumlah;
        $detail->pesanan_id = $idPesanan;
        $detail->save();

        return redirect()->back()->with('alert', 'Menu berhasil ditambahkah ke pesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::all();
        $pesanan = Pesanan::find($id);
        $detailPesanan = DetailPesanan::where('pesanan_id', $id)->get();

        return view('admin.menu.main', compact(['menu', 'id', 'pesanan', 'detailPesanan']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);

        return view('admin.menu.edit', compact(['menu']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "nama" => "required",
            "harga" => "required"
        ]);

        $menu = Menu::find($id);
        $menu->nama = $request->nama;
        $harga = $request->harga;
        if(strpos($harga, '.') == true) {
            $hargaDb = str_replace('.', '', $harga);
            $menu->harga = $hargaDb;
        } else {
            $menu->harga = $request->harga;
        }
        $menu->save();

        return redirect()->route('indexMenu')->with('alert', 'Berhasil edit menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();

        return redirect()->route('indexMenu')->with('alert', 'Berhasil delete menu');
    }
}
