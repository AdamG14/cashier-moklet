<?php

use Faker\Generator as Faker;
use App\DetailPesanan;

$factory->define(DetailPesanan::class, function (Faker $faker) {
    static $jumlah = 1;
    return [
        'menu_id' => 1,
        'jumlah' => $jumlah++,
        'pesanan_id' => 1,
    ];
});
