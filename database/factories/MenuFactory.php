<?php

use Faker\Generator as Faker;

$factory->define(App\Menu::class, function (Faker $faker) {
    static $no = 1;
    return [
        'nama' => 'Makanan'. $no++,
        'harga' => 10000,
    ];
});
