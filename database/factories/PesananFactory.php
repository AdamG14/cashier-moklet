<?php

use Faker\Generator as Faker;

$factory->define(App\Pesanan::class, function (Faker $faker) {
    return [
        'nama_pelanggan' => 'Adam Ghirvan Fadhil',
        'meja_id' => '1',
        'status' => true,
    ];
});
