<?php

use Faker\Generator as Faker;

$factory->define(App\Transaksi::class, function (Faker $faker) {
    return [
        'pesanan_id' => 1,
        'total' => 10000,
        'bayar' => 20000
    ];
});
