<?php

use Illuminate\Database\Seeder;
use App\Meja;

class MejasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Meja::class, 5)->create();
    }
}
