<?php

use Illuminate\Database\Seeder;
use App\DetailPesanan;


class DetailPesananTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DetailPesanan::class, 3)->create();
    }
}
